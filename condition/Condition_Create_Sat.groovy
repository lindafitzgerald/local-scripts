def subtask = issue.getSubTaskObjects().size<10
if(!subtask) {
    passesCondition = false // show create Satellite if greater than 10
}else {
    passesCondition = true // hide if less than 10
}