def subtask = issue.getSubTaskObjects().size<10
if(!subtask) {
    passesCondition = true // if greater than 10
}else {
    passesCondition = false // if less than 10
}