import com.atlassian.jira.issue.Issue
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.fields.CustomField;
import com.onresolve.jira.groovy.user.FieldBehaviours;
import com.onresolve.jira.groovy.user.FormField;
import com.opensymphony.workflow.InvalidInputException;

def issueManager = ComponentAccessor.getIssueManager()
//Issue issue = issueManager.getIssueByKeyIgnoreCase("TIS-14")

def testSelect = ComponentAccessor.getCustomFieldManager().getCustomFieldObject("customfield_10504")
def testSelectVal = issue.getCustomFieldValue(testSelect)?.toString()

if (testSelectVal != "Yes") {
invalidInputException = new InvalidInputException("customfield_10504 Test should be Yes")
}
