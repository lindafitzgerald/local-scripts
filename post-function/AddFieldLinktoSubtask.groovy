import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.user.util.UserUtil
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.link.IssueLink
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.comments.CommentManager
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.IssueInputParametersImpl
import com.atlassian.jira.web.action.issue.DeleteIssue
import com.atlassian.jira.util.JiraUtils

import org.apache.log4j.Logger
import org.apache.log4j.Level

def log = Logger.getLogger("com.acme.createIssue")
log.setLevel(Level.DEBUG)

def changeHolder = new DefaultIssueChangeHolder()
def issueManager = ComponentAccessor.getIssueManager()

def issueFactory = ComponentAccessor.getIssueFactory()
def cfManager = ComponentAccessor.getCustomFieldManager()
def linkMgr = ComponentAccessor.getIssueLinkManager()

def linkField = ComponentAccessor.getCustomFieldManager().getCustomFieldObject("customfield_10600")


//Issue issue = ComponentAccessor.getIssueManager().getIssueByKeyIgnoreCase("TIS-34")

def tisProject = ComponentAccessor.getProjectManager().getProjectObj(10100) //1
//ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
//def currentUser = ComponentAccessor.getJiraAuthenticationContext().getUser()

def myuser = ComponentAccessor.getUserUtil().getUserByName("linda.fitzgerald")
ComponentAccessor.getJiraAuthenticationContext().setLoggedInUser(myuser)


def links = linkMgr.getInwardLinks(issue.id)?.find{it.getLinkTypeId() == 10401}  //outward
def linkIssue = links.getSourceObject()

//def links = linkMgr.getInwardLinks(issue.id)?.find{it.getLinkTypeId() == 10400}  //inward
//def linkIssue = links.getSourceObject()

def linkIssueField = linkIssue.getCustomFieldValue(linkField)
//return linkIssueField
//Set Custom field in subtask with the linked issue field value
def subtasks = issue.getSubTaskObjects()
subtasks.each{ sub ->
    linkField.updateValue(null,sub, new ModifiedValue(sub.getCustomFieldValue(linkField),linkIssueField),changeHolder)
    //issueManager.updateIssue(myuser, sub, EventDispatchOption.DO_NOT_DISPATCH, false)
}