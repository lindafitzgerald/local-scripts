import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder

def specificationField = ComponentAccessor.getCustomFieldManager().getCustomFieldObject("customfield_10503")
def var="Update"

def changeHolder = new DefaultIssueChangeHolder()
def issueManager = ComponentAccessor.getIssueManager()




//In Case of Launch we update the specifications
specificationField.updateValue(null,issue, new ModifiedValue(issue.getCustomFieldValue(specificationField), var),changeHolder)
